package cn.buaa.edu;

public class Cylinder extends Cone {
    protected Cylinder(double radius, double height) {
        super(radius,radius,height);
    }

    public double getRadius() {
        return super.getRadius1();
    }

    public double getHeight() {
        return super.getHeight();
    }

    @Override
    public String getTypeInfo() {
        return Cylinder.TYPE_INFO + " "
                + this.getRadius() + " "
                + this.getHeight();
    }

    public static final String TYPE_INFO = "2.1";

    public static ISolidFactory getFactory() {
        return new ISolidFactory() {
            @Override
            public Solid create(String[] params) throws NumberFormatException {
                double radius = Double.parseDouble(params[1]);
                double height = Double.parseDouble(params[2]);
                return new Cylinder(radius,height);
            }
        };
    }
}
