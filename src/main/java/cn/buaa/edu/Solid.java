package cn.buaa.edu;

public abstract class Solid {

    public abstract double getVolume();

    public abstract String getTypeInfo();

    public static final String TYPE_INFO = "0";

    @Override
    public boolean equals(Object obj) {
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        return ((Solid)obj).getTypeInfo().equals(this.getTypeInfo());
    }

    @Override
    public int hashCode() {
        return this.getTypeInfo().hashCode();
    }
}
