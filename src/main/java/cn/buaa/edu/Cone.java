package cn.buaa.edu;

public class Cone extends Solid {
    private double volume;
    private double radius1;
    private double radius2;
    private double height;

    protected Cone(double radius1, double radius2, double height) {
        this.radius1 = radius1;
        this.radius2 = radius2;
        this.height = height;
        this.resetVolume();
    }

    protected final void resetVolume() {
        double s = radius1 * radius2 + radius2 * radius2 + radius1 * radius1;
        this.volume = Math.PI * height * s / 3.0;
    }

    @Override
    public double getVolume() {
        return volume;
    }

    public double getRadius1() {
        return radius1;
    }

    public double getRadius2() {
        return radius2;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public String getTypeInfo() {
        return Cone.TYPE_INFO + " "
                + this.radius1 + " "
                + this.radius2 + " "
                + this.height;
    }

    /*
    @Override
    public boolean equals(Object obj) {
        if (!this.getClass().equals(obj.getClass())) {
            return false;
        }
        Cone transform = (Cone)obj;
        return transform.height == this.height &&
                transform.radius1 == this.radius1 &&
                transform.radius2 == this.radius2;
    }
    */

    public static final String TYPE_INFO = "2";

    public static ISolidFactory getFactory() {
        return new ISolidFactory() {
            @Override
            public Solid create(String[] params) throws NumberFormatException {
                double radius1 = Double.parseDouble(params[1]);
                double radius2 = Double.parseDouble(params[2]);
                double height = Double.parseDouble(params[3]);
                return new Cone(radius1,radius2,height);
            }
        };
    }
}
