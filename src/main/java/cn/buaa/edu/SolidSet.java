package cn.buaa.edu;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

public class SolidSet implements Iterable {
    private HashSet<Solid> hashSet;
    private Solid maxSolid;
    private double sumVolume = 0;

    public SolidSet() {
        this.hashSet = new HashSet<Solid>();
        this.maxSolid = null;
    }

    public void add(Solid solid) {
        if (this.hashSet.add(solid)) {
            this.sumVolume += solid.getVolume();
        }
        if (this.maxSolid == null) {
            this.maxSolid = solid;
        }
        else if (this.maxSolid.getVolume() < solid.getVolume()) {
            this.maxSolid = solid;
        }
    }

    @Override
    public Iterator iterator() {
        return this.hashSet.iterator();
    }

    public double getSumVolume() {
        return this.sumVolume;
    }

    public double getMaxVolume() throws CheckSolidSetWhileEmptyException {
        if (this.maxSolid == null) {
            throw new CheckSolidSetWhileEmptyException();
        }
        return this.maxSolid.getVolume();
    }

    public String getMaxTypeInfo() throws CheckSolidSetWhileEmptyException {
        if (this.maxSolid == null) {
            throw new CheckSolidSetWhileEmptyException();
        }
        return this.maxSolid.getTypeInfo();
    }

    public SolidSet union(SolidSet other) {
        SolidSet result = new SolidSet();
        for (Solid solid : this.hashSet) {
            result.add(solid);
        }
        for (Solid solid : other.hashSet) {
            result.add(solid);
        }
        return result;
    }
}
