package cn.buaa.edu;

public class Cuboid extends Parallelepiped {
    protected Cuboid(double length,double width,double height) {
        super(length,0,width,0,0,height);
    }

    public double getLength() {
        return super.getAlphaX();
    }

    public double getWidth() {
        return super.getBetaY();
    }

    public double getHeight() {
        return super.getGamaZ();
    }

    @Override
    public String getTypeInfo() {
        return Cuboid.TYPE_INFO + " "
                + this.getLength() + " "
                + this.getWidth() + " "
                + this.getHeight();
    }

    public static final String TYPE_INFO = "1.1";

    public static ISolidFactory getFactory() {
        return new ISolidFactory() {
            @Override
            public Solid create(String[] params) throws NumberFormatException {
                double length = Double.parseDouble(params[1]);
                double width = Double.parseDouble(params[2]);
                double height = Double.parseDouble(params[3]);
                return new Cuboid(length,width,height);
            }
        };
    }
}
