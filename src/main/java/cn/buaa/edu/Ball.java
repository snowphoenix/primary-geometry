package cn.buaa.edu;

public class Ball extends Solid {
    private double radius;
    private double volume;

    protected Ball(double radius) {
        this.radius = radius;
        this.resetVolume();
    }

    protected final void resetVolume() {
        this.volume = 4.0 / 3.0 * Math.PI * radius * radius * radius;
    }

    @Override
    public double getVolume() {
        return volume;
    }

    @Override
    public String getTypeInfo() {
        return Ball.TYPE_INFO + " " + this.radius;
    }

    /*
    @Override
    public boolean equals(Object obj) {
        if (!this.getClass().equals(obj.getClass())) {
            return false;
        }
        Ball transform = (Ball)obj;
        return transform.radius == this.radius;
    }
     */

    public static final String TYPE_INFO = "3";

    public static ISolidFactory getFactory() {
        return new ISolidFactory() {
            @Override
            public Solid create(String[] params) throws NumberFormatException {
                return new Ball(Double.parseDouble(params[1]));
            }
        };
    }
}
