package cn.buaa.edu;

public class Parallelepiped extends Solid {
    private double alphaX;
    private double betaX;
    private double betaY;
    private double gamaX;
    private double gamaY;
    private double gamaZ;
    private double volume;

    protected Parallelepiped(
            double alphaX,
            double betaX,
            double betaY,
            double gamaX,
            double gamaY,
            double gamaZ) {
        this.alphaX = alphaX;
        this.betaX = betaX;
        this.betaY = betaY;
        this.gamaX = gamaX;
        this.gamaY = gamaY;
        this.gamaZ = gamaZ;
        this.resetVolume();
    }

    protected void resetVolume() {
        this.volume = Math.abs(alphaX * betaY * gamaZ);
    }

    public double getVolume() {
        return this.volume;
    }

    public double getAlphaX() {
        return alphaX;
    }

    public double getBetaX() {
        return betaX;
    }

    public double getBetaY() {
        return betaY;
    }

    public double getGamaX() {
        return gamaX;
    }

    public double getGamaY() {
        return gamaY;
    }

    public double getGamaZ() {
        return gamaZ;
    }

    public String getTypeInfo() {
        return Parallelepiped.TYPE_INFO + " "
                + this.alphaX + " "
                + this.betaX + " "
                + this.betaY + " "
                + this.gamaX + " "
                + this.gamaY + " "
                + this.gamaZ;
    }
    /*
    @Override
    public boolean equals(Object obj) {
        if (!this.getClass().equals(obj.getClass())) {
            return false;
        }
        Parallelepiped transform = (Parallelepiped) obj;
        return transform.alphaX == this.alphaX &&
                transform.betaX == this.betaX &&
                transform.betaY == this.betaY &&
                transform.gamaX == this.gamaX &&
                transform.gamaY == this.gamaY &&
                transform.gamaZ == this.gamaZ;
    }
    */

    public static final String TYPE_INFO = "1";

    public static ISolidFactory getFactory() {
        return new ISolidFactory() {
            @Override
            public Solid create(String[] params) throws NumberFormatException {
                double alphaX = Double.parseDouble(params[1]);
                double betaX = Double.parseDouble(params[2]);
                double betaY = Double.parseDouble(params[3]);
                double gamaX = Double.parseDouble(params[4]);
                double gamaY = Double.parseDouble(params[5]);
                double gamaZ = Double.parseDouble(params[6]);
                return new Parallelepiped(
                        alphaX,betaX,betaY,gamaX,gamaY,gamaZ
                );
            }
        };
    }
}
