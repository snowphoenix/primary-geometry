package cn.buaa.edu;

public interface ISolidFactory {
    Solid create(String[] params) throws NumberFormatException;
}
