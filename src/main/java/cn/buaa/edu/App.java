package cn.buaa.edu;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    private static ArrayList<SolidSet> solidSets = new ArrayList<SolidSet>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int arrayDepth = scanner.nextInt();
        for (int i = 0; i < arrayDepth; i++) {
            solidSets.add(new SolidSet());
        }
        int operateNumber = scanner.nextInt();
        for (int i = 0; i < operateNumber; i++) {
            int operate = scanner.nextInt();
            int index = scanner.nextInt() - 1;
            switch (operate) {
                case 1: {
                    getMaxVolume(index);
                    break;
                }
                case 2: {
                    getMaxTypeInfo(index);
                    break;
                }
                case 3: {
                    printSumVolume(index);
                    break;
                }
                case 4: {
                    //第一个会分出一个空字符串“”，要把它去掉
                    String[] params = scanner.nextLine().split(" ");
                    String[] trueParams = new String[params.length - 1];
                    System.arraycopy(params,1,trueParams,0,trueParams.length);
                    Solid newSolid = SolidFactory.getSolid(trueParams);
                    solidSets.get(index).add(newSolid);
                    break;
                }
                case 5: {
                    int index2 = scanner.nextInt() - 1;
                    SolidSet set1 = solidSets.get(index);
                    SolidSet set2 = solidSets.get(index2);
                    solidSets.add(set1.union(set2));
                    break;
                }
                default: {
                    break;
                }
            }
        }
    }

    private static void getMaxVolume(int index)
    {
        try {
            double maxVolume = solidSets.get(index).getMaxVolume();
            System.out.println(maxVolume);
        }
        catch (CheckSolidSetWhileEmptyException e) {
            System.out.println("Sorry, the set is empty!");
        }
    }

    private static void getMaxTypeInfo(int index)
    {
        try {
            String typeInfo = solidSets.get(index).getMaxTypeInfo();
            System.out.println(typeInfo);
        }
        catch (CheckSolidSetWhileEmptyException e) {
            System.out.println("Sorry, the set is empty!");
        }
    }

    private static void printSumVolume(int index)
    {
        double sumVolume = solidSets.get(index).getSumVolume();
        System.out.println(sumVolume);
    }
}
