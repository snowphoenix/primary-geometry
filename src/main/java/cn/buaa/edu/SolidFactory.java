package cn.buaa.edu;

import java.util.HashMap;
import java.util.Map;

public class SolidFactory {
    private SolidFactory(){}

    private static Map<String,ISolidFactory> factoryMap;

    static {
        factoryMap = new HashMap<String, ISolidFactory>();
        factoryMap.put(Parallelepiped.TYPE_INFO,Parallelepiped.getFactory());
        factoryMap.put(Cuboid.TYPE_INFO,Cuboid.getFactory());
        factoryMap.put(Cube.TYPE_INFO,Cube.getFactory());
        factoryMap.put(Cone.TYPE_INFO,Cone.getFactory());
        factoryMap.put(CircularCone.TYPE_INFO,CircularCone.getFactory());
        factoryMap.put(Cylinder.TYPE_INFO,Cylinder.getFactory());
        factoryMap.put(Ball.TYPE_INFO,Ball.getFactory());
    }

    public static Solid getSolid(String[] params) {
        try {
            return factoryMap.get(params[0]).create(params);
        }
        catch (NumberFormatException e) {
            System.err.println("params format is wrong");
            return null;
        }
        catch (NullPointerException e) {
            System.err.println("an nonexistent factory is required");
            return null;
        }
    }
}
