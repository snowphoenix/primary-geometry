package cn.buaa.edu;

public class Cube extends Cuboid {
    protected Cube(double length) {
        super(length,length,length);
    }

    @Override
    public String getTypeInfo() {
        return Cube.TYPE_INFO + " " + super.getLength();
    }

    public static final String TYPE_INFO = "1.1.1";

    public static ISolidFactory getFactory() {
        return new ISolidFactory() {
            @Override
            public Solid create(String[] params) throws NumberFormatException {
                return new Cube(Double.parseDouble(params[1]));
            }
        };
    }
}
