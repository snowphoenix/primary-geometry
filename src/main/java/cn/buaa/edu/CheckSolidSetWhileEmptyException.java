package cn.buaa.edu;


public class CheckSolidSetWhileEmptyException extends Exception {
    public CheckSolidSetWhileEmptyException() {
        super();
    }

    public CheckSolidSetWhileEmptyException(String message) {
        super(message);
    }

    public CheckSolidSetWhileEmptyException(String message, Exception inner) {
        super(message,inner);
    }
}
