package cn.buaa.edu;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    public void testCube()
    {
        Cube cube = new Cube(1) { };
        assertEquals(1.0, cube.getVolume());
    }

    public void testBall()
    {
        Ball ball = new Ball(1) { };
        assertEquals(4.0 / 3.0 * Math.PI, ball.getVolume());
    }

    public void testCone()
    {
        CircularCone cone = new CircularCone(1, 1) { };
        assertEquals(Math.PI / 3.0, cone.getVolume());
    }

    public void testFailCase()
    {
        assertTrue(false);
    }
}
